# CKSetup smoke test

Builds local .NET Core and ASP.NET Core projects from the solution,
then runs CKSetup on them, using various runtime and framework combinations.

Possible frameworks are loaded from the projects' TargetFrameworks property.

## Using

Close the `CKSetup-SmokeTest.sln` solution in Visual Studio before using, as it will try to auto-restore deleted directories.

In Powershell, call the [`run.ps1`](./run.ps1) script from this directory.

## Example output

Running the script gives an output similar to this:

```
01/13/2020 08:41:35: Deleting publish directory
01/13/2020 08:41:35: Clearing bin/obj directories
01/13/2020 08:41:35: Uninstalling CKSetup
01/13/2020 08:41:35: Installing CKSetup 10.2.2
01/13/2020 08:41:37: Calling dotnet restore
01/13/2020 08:41:38: dotnet restore successful
01/13/2020 08:41:38: Batch test starting.
01/13/2020 08:41:38: CKSetup.Test.App netcoreapp2.0 any: Calling: dotnet publish CKSetup.Test.App -f netcoreapp2.0 -o publish/CKSetup.Test.App-netcoreapp2.0-any
01/13/2020 08:41:41: CKSetup.Test.App netcoreapp2.0 any: dotnet publish succeeded
01/13/2020 08:41:41: CKSetup.Test.App netcoreapp2.0 any: Initializing database CKSetup.Test.App-netcoreapp2.0-any
01/13/2020 08:41:41: CKSetup.Test.App netcoreapp2.0 any: Calling CKSetup
01/13/2020 08:41:55: CKSetup.Test.App netcoreapp2.0 any: CKSetup FAILED
01/13/2020 08:41:55: CKSetup.Test.App netcoreapp2.0 any: Dropping database CKSetup.Test.App-netcoreapp2.0-any
01/13/2020 08:41:55: CKSetup.Test.App netcoreapp2.0 win10-x64: Calling: dotnet publish CKSetup.Test.App -f netcoreapp2.0 -o publish/CKSetup.Test.App-netcoreapp2.0-win10-x64 -r win10-x64
01/13/2020 08:41:58: CKSetup.Test.App netcoreapp2.0 win10-x64: dotnet publish succeeded
01/13/2020 08:41:58: CKSetup.Test.App netcoreapp2.0 win10-x64: Initializing database CKSetup.Test.App-netcoreapp2.0-win10-x64
01/13/2020 08:41:58: CKSetup.Test.App netcoreapp2.0 win10-x64: Calling CKSetup
01/13/2020 08:41:59: CKSetup.Test.App netcoreapp2.0 win10-x64: CKSetup FAILED
01/13/2020 08:41:59: CKSetup.Test.App netcoreapp2.0 win10-x64: Dropping database CKSetup.Test.App-netcoreapp2.0-win10-x64
01/13/2020 08:41:59: CKSetup.Test.App netcoreapp2.1 any: Calling: dotnet publish CKSetup.Test.App -f netcoreapp2.1 -o publish/CKSetup.Test.App-netcoreapp2.1-any
01/13/2020 08:42:01: CKSetup.Test.App netcoreapp2.1 any: dotnet publish succeeded
01/13/2020 08:42:01: CKSetup.Test.App netcoreapp2.1 any: Initializing database CKSetup.Test.App-netcoreapp2.1-any
01/13/2020 08:42:01: CKSetup.Test.App netcoreapp2.1 any: Calling CKSetup
01/13/2020 08:42:05: CKSetup.Test.App netcoreapp2.1 any: CKSetup succeeded
01/13/2020 08:42:05: CKSetup.Test.App netcoreapp2.1 any: Dropping database CKSetup.Test.App-netcoreapp2.1-any
01/13/2020 08:42:05: CKSetup.Test.App netcoreapp2.1 win10-x64: Calling: dotnet publish CKSetup.Test.App -f netcoreapp2.1 -o publish/CKSetup.Test.App-netcoreapp2.1-win10-x64 -r win10-x64
01/13/2020 08:42:07: CKSetup.Test.App netcoreapp2.1 win10-x64: dotnet publish succeeded
01/13/2020 08:42:07: CKSetup.Test.App netcoreapp2.1 win10-x64: Initializing database CKSetup.Test.App-netcoreapp2.1-win10-x64
01/13/2020 08:42:07: CKSetup.Test.App netcoreapp2.1 win10-x64: Calling CKSetup
01/13/2020 08:42:12: CKSetup.Test.App netcoreapp2.1 win10-x64: CKSetup succeeded
01/13/2020 08:42:12: CKSetup.Test.App netcoreapp2.1 win10-x64: Dropping database CKSetup.Test.App-netcoreapp2.1-win10-x64
01/13/2020 08:42:12: CKSetup.Test.App netcoreapp2.2 any: Calling: dotnet publish CKSetup.Test.App -f netcoreapp2.2 -o publish/CKSetup.Test.App-netcoreapp2.2-any
01/13/2020 08:42:14: CKSetup.Test.App netcoreapp2.2 any: dotnet publish succeeded
01/13/2020 08:42:14: CKSetup.Test.App netcoreapp2.2 any: Initializing database CKSetup.Test.App-netcoreapp2.2-any
01/13/2020 08:42:14: CKSetup.Test.App netcoreapp2.2 any: Calling CKSetup
01/13/2020 08:42:19: CKSetup.Test.App netcoreapp2.2 any: CKSetup succeeded
01/13/2020 08:42:19: CKSetup.Test.App netcoreapp2.2 any: Dropping database CKSetup.Test.App-netcoreapp2.2-any
01/13/2020 08:42:19: CKSetup.Test.App netcoreapp2.2 win10-x64: Calling: dotnet publish CKSetup.Test.App -f netcoreapp2.2 -o publish/CKSetup.Test.App-netcoreapp2.2-win10-x64 -r win10-x64
01/13/2020 08:42:21: CKSetup.Test.App netcoreapp2.2 win10-x64: dotnet publish succeeded
01/13/2020 08:42:21: CKSetup.Test.App netcoreapp2.2 win10-x64: Initializing database CKSetup.Test.App-netcoreapp2.2-win10-x64
01/13/2020 08:42:21: CKSetup.Test.App netcoreapp2.2 win10-x64: Calling CKSetup
01/13/2020 08:42:25: CKSetup.Test.App netcoreapp2.2 win10-x64: CKSetup succeeded
01/13/2020 08:42:25: CKSetup.Test.App netcoreapp2.2 win10-x64: Dropping database CKSetup.Test.App-netcoreapp2.2-win10-x64
01/13/2020 08:42:25: CKSetup.Test.App netcoreapp3.0 any: Calling: dotnet publish CKSetup.Test.App -f netcoreapp3.0 -o publish/CKSetup.Test.App-netcoreapp3.0-any
01/13/2020 08:42:27: CKSetup.Test.App netcoreapp3.0 any: dotnet publish succeeded
01/13/2020 08:42:27: CKSetup.Test.App netcoreapp3.0 any: Initializing database CKSetup.Test.App-netcoreapp3.0-any
01/13/2020 08:42:27: CKSetup.Test.App netcoreapp3.0 any: Calling CKSetup
01/13/2020 08:42:28: CKSetup.Test.App netcoreapp3.0 any: CKSetup FAILED
01/13/2020 08:42:28: CKSetup.Test.App netcoreapp3.0 any: Dropping database CKSetup.Test.App-netcoreapp3.0-any
01/13/2020 08:42:28: CKSetup.Test.App netcoreapp3.0 win10-x64: Calling: dotnet publish CKSetup.Test.App -f netcoreapp3.0 -o publish/CKSetup.Test.App-netcoreapp3.0-win10-x64 -r win10-x64
01/13/2020 08:42:31: CKSetup.Test.App netcoreapp3.0 win10-x64: dotnet publish succeeded
01/13/2020 08:42:31: CKSetup.Test.App netcoreapp3.0 win10-x64: Initializing database CKSetup.Test.App-netcoreapp3.0-win10-x64
01/13/2020 08:42:31: CKSetup.Test.App netcoreapp3.0 win10-x64: Calling CKSetup
01/13/2020 08:42:32: CKSetup.Test.App netcoreapp3.0 win10-x64: CKSetup FAILED
01/13/2020 08:42:32: CKSetup.Test.App netcoreapp3.0 win10-x64: Dropping database CKSetup.Test.App-netcoreapp3.0-win10-x64
01/13/2020 08:42:32: CKSetup.Test.App netcoreapp3.1 any: Calling: dotnet publish CKSetup.Test.App -f netcoreapp3.1 -o publish/CKSetup.Test.App-netcoreapp3.1-any
01/13/2020 08:42:34: CKSetup.Test.App netcoreapp3.1 any: dotnet publish succeeded
01/13/2020 08:42:34: CKSetup.Test.App netcoreapp3.1 any: Initializing database CKSetup.Test.App-netcoreapp3.1-any
01/13/2020 08:42:34: CKSetup.Test.App netcoreapp3.1 any: Calling CKSetup
01/13/2020 08:42:35: CKSetup.Test.App netcoreapp3.1 any: CKSetup FAILED
01/13/2020 08:42:35: CKSetup.Test.App netcoreapp3.1 any: Dropping database CKSetup.Test.App-netcoreapp3.1-any
01/13/2020 08:42:35: CKSetup.Test.App netcoreapp3.1 win10-x64: Calling: dotnet publish CKSetup.Test.App -f netcoreapp3.1 -o publish/CKSetup.Test.App-netcoreapp3.1-win10-x64 -r win10-x64
01/13/2020 08:42:37: CKSetup.Test.App netcoreapp3.1 win10-x64: dotnet publish succeeded
01/13/2020 08:42:37: CKSetup.Test.App netcoreapp3.1 win10-x64: Initializing database CKSetup.Test.App-netcoreapp3.1-win10-x64
01/13/2020 08:42:37: CKSetup.Test.App netcoreapp3.1 win10-x64: Calling CKSetup
01/13/2020 08:42:37: CKSetup.Test.App netcoreapp3.1 win10-x64: CKSetup FAILED
01/13/2020 08:42:37: CKSetup.Test.App netcoreapp3.1 win10-x64: Dropping database CKSetup.Test.App-netcoreapp3.1-win10-x64
01/13/2020 08:42:37: CKSetup.Test.AspNetCore netcoreapp2.2 any: Calling: dotnet publish CKSetup.Test.AspNetCore -f netcoreapp2.2 -o publish/CKSetup.Test.AspNetCore-netcoreapp2.2-any
01/13/2020 08:42:40: CKSetup.Test.AspNetCore netcoreapp2.2 any: dotnet publish succeeded
01/13/2020 08:42:40: CKSetup.Test.AspNetCore netcoreapp2.2 any: Initializing database CKSetup.Test.AspNetCore-netcoreapp2.2-any
01/13/2020 08:42:40: CKSetup.Test.AspNetCore netcoreapp2.2 any: Calling CKSetup
01/13/2020 08:42:44: CKSetup.Test.AspNetCore netcoreapp2.2 any: CKSetup succeeded
01/13/2020 08:42:44: CKSetup.Test.AspNetCore netcoreapp2.2 any: Dropping database CKSetup.Test.AspNetCore-netcoreapp2.2-any
01/13/2020 08:42:44: CKSetup.Test.AspNetCore netcoreapp2.2 win10-x64: Calling: dotnet publish CKSetup.Test.AspNetCore -f netcoreapp2.2 -o publish/CKSetup.Test.AspNetCore-netcoreapp2.2-win10-x64 -r win10-x64
01/13/2020 08:42:47: CKSetup.Test.AspNetCore netcoreapp2.2 win10-x64: dotnet publish succeeded
01/13/2020 08:42:47: CKSetup.Test.AspNetCore netcoreapp2.2 win10-x64: Initializing database CKSetup.Test.AspNetCore-netcoreapp2.2-win10-x64
01/13/2020 08:42:48: CKSetup.Test.AspNetCore netcoreapp2.2 win10-x64: Calling CKSetup
01/13/2020 08:42:53: CKSetup.Test.AspNetCore netcoreapp2.2 win10-x64: CKSetup succeeded
01/13/2020 08:42:53: CKSetup.Test.AspNetCore netcoreapp2.2 win10-x64: Dropping database CKSetup.Test.AspNetCore-netcoreapp2.2-win10-x64
01/13/2020 08:42:53: CKSetup.Test.AspNetCore netcoreapp3.1 any: Calling: dotnet publish CKSetup.Test.AspNetCore -f netcoreapp3.1 -o publish/CKSetup.Test.AspNetCore-netcoreapp3.1-any
01/13/2020 08:42:55: CKSetup.Test.AspNetCore netcoreapp3.1 any: dotnet publish succeeded
01/13/2020 08:42:55: CKSetup.Test.AspNetCore netcoreapp3.1 any: Initializing database CKSetup.Test.AspNetCore-netcoreapp3.1-any
01/13/2020 08:42:56: CKSetup.Test.AspNetCore netcoreapp3.1 any: Calling CKSetup
01/13/2020 08:42:56: CKSetup.Test.AspNetCore netcoreapp3.1 any: CKSetup FAILED
01/13/2020 08:42:56: CKSetup.Test.AspNetCore netcoreapp3.1 any: Dropping database CKSetup.Test.AspNetCore-netcoreapp3.1-any
01/13/2020 08:42:56: CKSetup.Test.AspNetCore netcoreapp3.1 win10-x64: Calling: dotnet publish CKSetup.Test.AspNetCore -f netcoreapp3.1 -o publish/CKSetup.Test.AspNetCore-netcoreapp3.1-win10-x64 -r win10-x64
01/13/2020 08:43:00: CKSetup.Test.AspNetCore netcoreapp3.1 win10-x64: dotnet publish succeeded
01/13/2020 08:43:00: CKSetup.Test.AspNetCore netcoreapp3.1 win10-x64: Initializing database CKSetup.Test.AspNetCore-netcoreapp3.1-win10-x64
01/13/2020 08:43:00: CKSetup.Test.AspNetCore netcoreapp3.1 win10-x64: Calling CKSetup
01/13/2020 08:43:00: CKSetup.Test.AspNetCore netcoreapp3.1 win10-x64: CKSetup FAILED
01/13/2020 08:43:00: CKSetup.Test.AspNetCore netcoreapp3.1 win10-x64: Dropping database CKSetup.Test.AspNetCore-netcoreapp3.1-win10-x64
01/13/2020 08:43:00: Batch test complete.

Project                 Framework     Runtime   PublishSuccess CKSetupSuccess
-------                 ---------     -------   -------------- --------------
CKSetup.Test.App        netcoreapp2.0 any                 True          False
CKSetup.Test.App        netcoreapp2.0 win10-x64           True          False
CKSetup.Test.App        netcoreapp2.1 any                 True           True
CKSetup.Test.App        netcoreapp2.1 win10-x64           True           True
CKSetup.Test.App        netcoreapp2.2 any                 True           True
CKSetup.Test.App        netcoreapp2.2 win10-x64           True           True
CKSetup.Test.App        netcoreapp3.0 any                 True          False
CKSetup.Test.App        netcoreapp3.0 win10-x64           True          False
CKSetup.Test.App        netcoreapp3.1 any                 True          False
CKSetup.Test.App        netcoreapp3.1 win10-x64           True          False
CKSetup.Test.AspNetCore netcoreapp2.2 any                 True           True
CKSetup.Test.AspNetCore netcoreapp2.2 win10-x64           True           True
CKSetup.Test.AspNetCore netcoreapp3.1 any                 True          False
CKSetup.Test.AspNetCore netcoreapp3.1 win10-x64           True          False
```