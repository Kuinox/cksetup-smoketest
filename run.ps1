# Builds .NET Core and ASP.NET Core projects,
# then runs CKSetup on it, using various runtime and framework combinations.
# Possible frameworks are loaded from the project's TargetFrameworks property.

# The CKSetup version to use.
# The global CKSetup will be uninstalled, then reinstalled to match this version.
$cksetupVersion = "11.0.0-a00-00-0021-develop"

# The runtimes to use. 'any' will publish a framework-dependent BinPath,
# whereas anything else will publish a self-contained BinPath.
$runtimes = @(
    'any'
    'win10-x64'
);

# The projects to use CKSetup on.
$projects = @(
    'CKSetup.Test.App'
    'CKSetup.Test.AspNetCore'
);

# The output directory of dotnet publish, and root of all BinPaths,
# working directories, CKSetup XML files, and logs.
$publishRoot = "publish"

# Map of supported target runtimes to use in CKSetup.xml's PreferredTargetRuntimes.
$tgtRuntimes = @{
    'netcoreapp2.0' = 'NetCoreApp20';
    'netcoreapp2.1' = 'NetCoreApp21';
    'netcoreapp2.2' = 'NetCoreApp22';
    'netcoreapp3.0' = 'NetCoreApp30';
    'netcoreapp3.1' = 'NetCoreApp31';
}

$table = New-Object system.Data.DataTable "Results"
$table.columns.add( (New-Object system.Data.DataColumn Project,([string])) )
$table.columns.add( (New-Object system.Data.DataColumn Framework,([string])) )
$table.columns.add( (New-Object system.Data.DataColumn Runtime,([string])) )
$table.columns.add( (New-Object system.Data.DataColumn PublishSuccess,([bool])) )
$table.columns.add( (New-Object system.Data.DataColumn CKSetupSuccess,([bool])) )

Write-Host "$(Get-Date): Deleting $publishRoot directory"
Remove-Item $publishRoot -Recurse -ErrorAction Ignore

Write-Host "$(Get-Date): Clearing bin/obj directories"
get-childitem -Include .vs,bin,obj -Recurse -force | ? { $_.FullName -inotmatch 'node_modules' } | Remove-Item -Force -Recurse | Out-Null

Write-Host "$(Get-Date): Clearing CKSetupStore"
Remove-Item "$($env:LOCALAPPDATA)\CKSetupStore" -Recurse -ErrorAction Ignore

Write-Host "$(Get-Date): Uninstalling CKSetup"
dotnet tool uninstall -g CKSetup | Out-Null

Write-Host "$(Get-Date): Installing CKSetup $cksetupVersion"
dotnet tool install -g CKSetup --version $cksetupVersion | Out-Null

Write-Host "$(Get-Date): Calling dotnet restore"
dotnet restore | Out-Null
if($?) {
    Write-Host "$(Get-Date): dotnet restore successful" -ForegroundColor Green
    $cksetupSuccess = $true;
} else {
    Write-Host "$(Get-Date): dotnet restore FAILED" -ForegroundColor Red
    exit 1;
}

Function Run-Test( $project, $framework, $runtime ) {
    $outDirName = "${project}-${framework}-${runtime}";
    $outDir = "$publishRoot/$outDirName";
    $xmlPath = "$publishRoot/$outDirName.xml";
    $logPath = "$publishRoot/$outDirName.log";
    $workingDirPath = "$outDirName-WorkingDirectory";
    $dbName = $outDirName;
    $tgtRuntime = $tgtRuntimes[$framework];
    $publishSuccess = $false;
    $cksetupSuccess = $false;
    if($runtime -eq 'any') {
        Write-Host "$(Get-Date): $project $framework ${runtime}: Calling: dotnet publish $project -f $framework -o $outDir"
        dotnet publish $project -f $framework -o $outDir | Out-Null
    } else {
        Write-Host "$(Get-Date): $project $framework ${runtime}: Calling: dotnet publish $project -f $framework -o $outDir -r $runtime"
        dotnet publish $project -f $framework -o $outDir -r $runtime | Out-Null
    }
    if($?) {
        $publishSuccess = $true;
        Write-Host "$(Get-Date): $project $framework ${runtime}: dotnet publish succeeded" -ForegroundColor Green
        Write-Host "$(Get-Date): $project $framework ${runtime}: Initializing database ${dbName}"
        invoke-sqlcmd -ServerInstance "." -Query "DROP DATABASE IF EXISTS [${dbName}];"
        invoke-sqlcmd -ServerInstance "." -Query "CREATE DATABASE [${dbName}];"
        
        Write-Host "$(Get-Date): $project $framework ${runtime}: Calling CKSetup"
        $cksetupXml = @"
<Setup Engine="CK.Setup.StObjEngine, CK.StObj.Engine">
    <Aspect Type="CK.Setup.SetupableAspectConfiguration, CK.Setupable.Model" Version="1"></Aspect>
    <Aspect Type="CK.Setup.SqlSetupAspectConfiguration, CK.SqlServer.Setup.Model">
        <DefaultDatabaseConnectionString>Server=.;Database=${dbName};Integrated Security=SSPI</DefaultDatabaseConnectionString>
    </Aspect>
    <BinPaths>
        <BinPath Path="${outDirName}"></BinPath>
    </BinPaths>
    <WorkingDirectory>${workingDirPath}</WorkingDirectory>
    <PreferredTargetRuntimes>${tgtRuntime}</PreferredTargetRuntimes>
</Setup>
"@;
        $cksetupXml | Out-File $xmlPath
        & cksetup run $xmlPath -v Debug -l $logPath 2>&1 | Out-Null
        if($?) {
            Write-Host "$(Get-Date): $project $framework ${runtime}: CKSetup succeeded" -ForegroundColor Green
            $cksetupSuccess = $true;
        } else {
            Write-Host "$(Get-Date): $project $framework ${runtime}: CKSetup FAILED" -ForegroundColor Red
        }
        Write-Host "$(Get-Date): $project $framework ${runtime}: Dropping database ${dbName}"
        invoke-sqlcmd -ServerInstance "." -Query "DROP DATABASE IF EXISTS [${dbName}];"
    } else {
        Write-Host "$(Get-Date): $project $framework ${runtime}: dotnet publish FAILED" -ForegroundColor Red
    }

    $row = $table.NewRow()
    $row.Project = $project;
    $row.Framework = $framework;
    $row.Runtime = $runtime;
    $row.PublishSuccess = $publishSuccess;
    $row.CKSetupSuccess = $cksetupSuccess;
    $table.Rows.Add($row)
}

Function Get-TargetFrameworks-FromProject( $project ) {
    $Path =  "$project/$project.csproj"
    $Xml = [xml](Get-Content $Path)
    $XPathResult = Select-Xml -Xml $Xml -XPath "/Project/PropertyGroup/TargetFrameworks"
    return $XPathResult.Node.InnerText.Split(";");
}

Remove-Item "publish" -Recurse -ErrorAction Ignore

Write-Host "$(Get-Date): Batch test starting."
foreach( $project in $projects ) {
    $frameworks = Get-TargetFrameworks-FromProject $project
    foreach( $framework in $frameworks ) {
        foreach( $runtime in $runtimes ) {
            Run-Test $project $framework $runtime
        }
    }
}
Write-Host "$(Get-Date): Batch test complete." -ForegroundColor Green
$table | format-table -AutoSize
